.import fbx.web 1.0 as Web
.import fbx.async 1.0 as Async

function updateEpisodes(url, lang, infos_cached) {

    if (infos_cached && infos_cached.length > 0) {
        try {
            var infos = JSON.parse(infos_cached);
            if (infos.lang === lang && infos.episodes.length > 0)
                return Async.Deferred.resolved({updated: false});
        }
        catch (e) {}
    }

    // no cache valid
    return Web.Http.Transaction.factory({
        'method': 'GET',
        'url': url + "/episodes.json",
        'headers': {
        'Accept': 'application/json'
        }}).send().then(function (rsp) {
            return rsp.jsonParse();
        }).then(function (jsonResponse) {
            return jsonResponse.data;
        }).then(function(r) {
            var infos = {lang: lang, episodes:[], timestamp: new Date().toISOString()}
            for (var ei = 0; ei < r.length; ei++) {
               if (r[ei].translated_languages && r[ei].translated_languages.indexOf(lang)) {
                  infos.episodes.push({name: r[ei].name, total_pages: r[ei].total_pages})
               }
            }
            console.log("refresh episodes " + JSON.stringify(infos))
            return {updated: true, data: JSON.stringify(infos)};
        });
}

function getPageUrl(episode, thepage, cover) {
    var page = thepage
    var ep = episode

    var episodeNumber = ep.substr(2,2);
    var pageNumberStr = page < 10? "0" + page: "" + page
    var imgurl;

    console.log("episode " + ep + " (" + episodeNumber + ") page " + page)

    if (page == 0 && cover) {
        imgurl = app.url + "/" + ep + "/low-res/" + lang +
                "_Pepper-and-Carrot_by-David-Revoy_E" + episodeNumber + ".jpg"
    }
    else {
        imgurl = app.url + "/" + ep + "/low-res/" + lang +
                "_Pepper-and-Carrot_by-David-Revoy_E" + episodeNumber +
                "P" + pageNumberStr + ".jpg"
    }
    return imgurl;
}
