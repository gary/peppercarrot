import QtQuick 2.5
import fbx.application 1.0
import "peppercarrot.js" as Pepper
//import fbx.web 1.0
//import fbx.async 1.0
//import fbx.ui.layout 1.0
//import fbx.ui.control 2.0

Application {
    id: app

    property string url: "https://www.peppercarrot.com/0_sources"
    property string lang: "fr"

    property var albums: {}

    Settings {
        id: settings

        property string episodesInfo
        property string episodeName
        property int pageNumber

        onReady: {
            Pepper.updateEpisodes(
                url, lang, episodesInfo).then(function(res) {
                if (res.updated) {
                    episodesInfo = res.data
                }

                albums = JSON.parse(episodesInfo);
                page.setInitialPage(
                        { episode: settings.episodeName,
                          page: settings.pageNumber})
            },
            function(err) {console.error("error " + err)});
        }
    }

    function handleUrl(action, url, mimeType)
    {
    }

    FocusScope {
        id: container
        anchors.fill: parent
        focus: true

        Rectangle {
            anchors.fill: parent
            color: "white"
        }

        Flickable {
            id: viewport
            anchors.fill: parent
            contentWidth: page.width
            contentHeight: page.height
            clip: true
            boundsBehavior: Flickable.StopAtBounds
            flickDeceleration: 100
            maximumFlickVelocity: 500

            onFlickStarted: print("flick started")
            onFlickEnded: print("flick ended")

            Image {
                property var pageinfo

                id: page
                fillMode: Image.PreserveAspectFit
                width: viewport.width
                height: sourceSize.width >= sourceSize.height ? viewport.height:
                    Math.ceil(sourceSize.height * viewport.width / sourceSize.width)
                source: "Young-Pepper_peppercarrot-wallpaper_by-David-Revoy.jpg"
                onStatusChanged: {
                    if (status == Image.Ready) {
                        console.log("loaded " + source)
                        if (pageinfo) {
                            if (settings.episodeName !== pageinfo.episode)
                                settings.episodeName = pageinfo.episode
                            if (settings.pageNumber !== pageinfo.page)
                                settings.pageNumber = pageinfo.page
                        }
                    }
                    if (status == Image.Error) console.error("loading error " + source)
                }
                onSourceChanged: viewport.contentY = 0

                function setInitialPage(info) {
                    pageinfo = info
                    pageinfo.index = -1
                    for (var ei = 0; ei < albums.episodes.length; ei++) {
                        if (pageinfo.episode === albums.episodes[ei].name) {
                            pageinfo.index = ei
                            break
                        }
                    }
                    if (pageinfo.index < 0) {
                        pageinfo.index = 0
                        pageinfo.episode = albums.episodes[pageinfo.index].name
                    }
                    pageinfo.last = albums.episodes[pageinfo.index].total_pages
                    pageinfo.page = Math.min(info.page, pageinfo.last)
                    if (pageinfo.page === 0) pageinfo.cover = true
                    else pageinfo.cover = false
                    source = Pepper.getPageUrl(pageinfo.episode, pageinfo.page, pageinfo.cover)
                }

                function nextPage() {
                    if (status == Image.Loading) return
                    if (! pageinfo) return
                    console.log(JSON.stringify(pageinfo))
                    if (pageinfo.page === 0 && pageinfo.cover) {
                        pageinfo.cover = false
                    } else if (pageinfo.page < pageinfo.last) {
                        pageinfo.page ++
                    } else if (albums.episodes[pageinfo.index + 1]) {
                        pageinfo.index ++
                        pageinfo.episode = albums.episodes[pageinfo.index].name
                        pageinfo.last = albums.episodes[pageinfo.index].total_pages
                        pageinfo.page = 0
                        pageinfo.cover = true
                    } else {
                        console.log("no more")
                        return
                    }
                    source = Pepper.getPageUrl(pageinfo.episode, pageinfo.page, pageinfo.cover)
                }
            }
        }

        Keys.onDownPressed: viewport.flick(0, 300)
        Keys.onUpPressed: viewport.flick(0, -300)
        Keys.onRightPressed: page.nextPage()
    }
}
