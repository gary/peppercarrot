# peppercarrot on freebox

An E-Reader of [Pepper&Carrot](https://www.peppercarrot.com/) for Freebox.

This project is written in QML.

# How to build

1. Clone this repository.
1. Install QT Creator from [Freebox SDK](https://dev.freebox.fr/sdk/).
1. Enable developer mode of the Freebox
1. Run

# Special Thanks

* David Revoy: for making this awesome webcomic and endorsing this repository.

# License
This project is licensed under GNU GPL 3.0 or higher.
